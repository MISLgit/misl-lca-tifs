
# Source code for: "Accurate and Efficient Foregery Detection Using Lateral Chromatic Aberration" 
by Owen Mayer and Matthew C. Stamm  
Deparment of Electrical and Computer Engineering  
Drexel University - Philadelphia, PA, USA


## About

This repository contains a MATLAB codebase for forensic analysis of digital images using an imaging trace called lateral chromatic aberration (LCA).
The functions within this repository perform the following tasks:
- Efficiently measure LCA in localized image regions, using a Diamond Search algorithm
- Estimate a global model fit of LCA for an entire image, using an iterative Guass-Newton method
- Calculate the forgery detection metric to determine if the LCA in an image region is statistically inconsistent with the rest of the image

This code in this repository was used to conduct the experiments the article title "Accurate and Efficient Foregery Detection Using Lateral Chromatic Aberration," which was published in the IEEE Transactions on Information Forensics and Security in July 2018. 

- [Link to article](http://misl.ece.drexel.edu/wp-content/uploads/2018/02/Mayer_TIFS_2018.pdf) (PDF)


## Installation
Very little installation is required.

1. Download the repository .zip or clone via git.
2. Open in MATLAB
3. Run any example in the "examples" folder


##### Prerequisites
MATLAB 

This has been tested on MATLAB 2017a on Ubuntu 16.04. Please let us know if there are any compatability issues.

## Citing this Code
If you are using this code for scholarly or academic research, please cite this paper:

Mayer, Owen, and Matthew C. Stamm. "Accurate and Efficient Image Forgery Detection Using Lateral Chromatic Aberration." *IEEE Transactions on Information Forensics and Security* (2018).

bibtex:
```
@article{mayer2018lca,
  title={Accurate and Efficient Image Forgery Detection Using Lateral Chromatic Aberration},
  author={Mayer, Owen and Stamm, Matthew C},
  journal={IEEE Transactions on Information Forensics and Security},
  year={2018},
  publisher={IEEE}
}
```

