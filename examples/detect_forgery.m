%   DETECT_FORGERY.M
%An example script that shows how detect a splicing forgery using LCA

%Author: Owen Mayer, Drexel Univesity, om82@drexel.edu
%26 Feb 2018

addpath('../src/') %Add source code to path
close all; clearvars; %close open figures and clear workspace

%% 0 Parameters
HW = 32; %Local LCA measurement window half-width (HW)
DELTA = 5; %Local LCA measurement maximum displacement
U = 10; %Local LCA measurement Upsample factor (U=10 if often a better choice but U=5 is used here to)

%% 1 Load Image
I = imread('images/001.jpg');


%% 2 Find Corners, Measure and Estimate Local and Global LCA
opts.nCorners = 4000;opts.cThresh=0.008; %Corner Parameters
opts.iRC=2; opts.hW = 32;opts.delta=5;opts.u=10; %Local LCA parameters
opts.dGamma=0.01;opts.maxiter=1000; opts.convergedThresh=10^-5; %Global parameters

%single wrapper function that performs steps 2-4 demonstrated in the
%show_LCA_displacements function.
[K,dhat,theta] = lca_corners_localDisp_globalParams(I,opts);

%check to make sure corner points are good
figure;
imshow(I); hold on;
plot(K(:,1),K(:,2),'mo')
%% 3 Calculate LCA Inconsistency Vector

%calculate inconsistency vector 
E = lcaInconsistency(K,dhat{1},theta{1},dhat{2},theta{2}); %EQ 13
%^Note this function is designed to handle exactly 2 color-channel-pairings

%% 4 Calculate Detection Metric Region-by-Region
boxWidth = 256; %box size to look at
boxOverlap = 128; %how much to overlap
min_pts_in_box = 2; %number of points in box to make a decision

[INDS,JNDS] = imoverlappingboxes(I,boxWidth,boxWidth,boxOverlap,boxOverlap); %Tile the image with overlapping boxes
%These will serve as different regions of interest

m = zeros(size(INDS,1),1);
for ii = 1:size(INDS,1)
    [~,~,iC] = PtsInBox(K,[INDS(ii,1) JNDS(ii,1) boxWidth boxWidth],1); %vector of inconsistency indices that are in the region of interest
    if sum(iC) >= min_pts_in_box %if there enough corner points in the box
        m(ii) = lca_detectionFeature_InOut(E,iC,~iC); %Calculate the detection metric
        %This is the main 
    else
        m(ii) = NaN;
    end
end


%%
threshold = 10000;
figure;
imshow(I); hold on;
hPatch = zeros(size(INDS,1),1);
 for ii = 1:size(INDS,1)
    xx = [INDS(ii,1) INDS(ii,end) INDS(ii,end) INDS(ii,1)];
    yy = [JNDS(ii,1) JNDS(ii,1) JNDS(ii,end) JNDS(ii,end)];
    hPatch(ii) = patch(xx,yy,[1 0.1 0.1]);
    if m(ii) > threshold
        set(hPatch(ii),'FaceAlpha',0.25,'EdgeColor','none')
%                   set(hPatch(ii),'FaceAlpha',
    else
        set(hPatch(ii),'FaceAlpha',0,'EdgeColor','none')
    end
end
