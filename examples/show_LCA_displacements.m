%   SHOW_LCA_DISPLACEMENTS.M
%An example script that shows how to estimate and visualize local and global LCA displacements

%Author: Owen Mayer, Drexel Univesity, om82@drexel.edu
%26 Feb 2018

addpath('../src/') %Add source code to path
close all; clearvars; %close open figures and clear workspace

%% 0 Parameters
HW = 32; %Local LCA measurement window half-width (HW)
DELTA = 5; %Local LCA measurement maximum displacement
U = 5; %Local LCA measurement Upsample factor (U=10 if often a better choice but U=5 is used here to)

%% 1 Load Image
I = imread('images/002.jpg');
hFig = figure;
imshow(I); %show image

%% 2 Select Key Points (Corners)
%Keypoints are the spatial locations at which LCA is measured. They require
%spatial gradient in 2 dimensions, so corner points are used

%Local LCA estimates require a measurement window and so no keypoints
%closer than half the window size plus potential offset can be used
mask = imedgemask(I,HW+DELTA+1); %create a mask that excludes the image edges. 

K = selectiveCorners_withMask(rgb2gray(I),mask,'MinimumEigenvalue',2*HW,0.01,1000);%Select corner points
%The last 3 parameters can be played around with. More corner points yields
%more information, but if they are too close together this information is
%redundant and also takes much longer 

%Display Corner Points
figure(hFig); hold on;
plot(K(:,1),K(:,2),'mo')

%% 3 Measure Local LCA

%Initialize 2D vectors to hold local LCA displacement measurements
dhat = zeros(size(K,1),2); %Green->Red LCA

wbar = waitbar(0,'Measuring Local LCA'); %progress bar
for ii = 1:size(K,1) %iterate through each keypoint
    dhat(ii,:) = localLCAdisplacement_fixedDS(I,2,1,K(ii,:),HW,U,DELTA); %use diamond search (Sec. III)
    waitbar(ii/size(K,1),wbar); %update progress bar
end
close(wbar)

hFig2 = figure;
figure(hFig2)
imshow(I); %show image
hold on;
plot_dLCA_vectorField(K,dhat,-200,'r');
title('image with G->R local LCA displacements');

%% 4 Estimate Global Fit

%Find global model parameters (theta) that best fit the local model
theta = estimateJFparamsGaussNewton(K,dhat,size(I),0.01,1000,10^-5,0,0); %Use Least Squares Gauss Newton Method
%Theta is 3D with elements: x position of optical center, y posistion of
%optical center, and expansion coefficietion. Eq (5)

[~, d] = JohnsonFaridLCAmodel(K,theta); %Global displacement model (Eq (2))
figure(hFig2);
plot_dLCA_vectorField(K,d,-200,'-b');
title('image with G->R local (red) and global fit (blue) LCA displacements');

